# Kubernetes Setup for Prometheus and Grafana

## Installing Prometheus from Helm

```bash
helm install stable/prometheus --name stable-prometheus -f helm/values/valuesPrometheus.yaml
```

This will create all prometheus components in the `default` default namespace

To check if the pods are running you can run:

```bash
kubectl get pods --namespace=default 
```
To shut down all components you can delete the helm release via:
```bash
helm del --purge stable-prometheus 
```

## Port forward Prometheus locally 

To port forward prometheus to localhost:9090 you can run:
```bash
 export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9090
```
## Installing Grafana from Helm

```bash
helm install stable/grafana --name stable-grafana -f helm/values/valuesGrafana.yaml
```

This will create all grafana components in the `default` default namespace

To check if the pods are running you can run:

```bash
kubectl get pods --namespace=default 
```
To shut down all components you can delete the helm release via:
```bash
helm del --purge stable-grafana 
```

## Port forward Grafana locally 

To port forward prometheus to localhost:3000 you can run:
```bash
export POD_NAME=$(kubectl get pods --namespace default -l "app=grafana" -o jsonpath="{.items[0].metadata.name}")   
     kubectl --namespace default port-forward $POD_NAME 3000
```

The user and password for grafana is by default set to admin/admin. This can be updated in grafanaValues.yaml